
/// Ad-Hoc description
/// Inputs n1 and n2, outputs are addition and subtraction (n1-n2)
/// Outputs are parameters passed with pointers (pure C-Style)
void sumAndSubPtr(const double n1, const double n2, double * sum, double * sub)
{
  (*sum) = n1 + n2;
  (*sub) = n1 - n2;
}

/// @brief Calculates Sum and Subtraction of two input doubles in a single function; Outputs are parameters passed with references (CPP-Style)
/// @param[in] n1 The first number to add and be subtracted from
/// @param[in] n2 The second number to add and be subtract (to the first number)
/// @param[out] sum Output parameter to store the result of the addition (n1+n2); passed by reference
/// @param[out] sub Output parameter to store the result of the subtraction (n1-n2); passed by reference
/// @return return is void (operations are always possible)
void sumAndSubRef(const double n1, const double n2, double & sum, double & sub)
{
  sum = n1 + n2;
  sub = n1 - n2;
}
