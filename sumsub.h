#ifndef SUBSUM_H
#define SUBSUM_H
// https://stackoverflow.com/questions/3246803/why-use-ifndef-class-h-and-define-class-h-in-h-file-but-not-in-cpp

// A versão C++ será #pragma once - https://en.wikipedia.org/wiki/Pragma_once


void sumAndSubPtr(const double n1, const double n2, double * sum, double * sub);

void sumAndSubRef(const double n1, const double n2, double & sum, double & sub);

#endif
