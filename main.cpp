// Software Project FEUP
// https://docs.microsoft.com/en-us/cpp/build/reference/creating-a-makefile-project?view=msvc-160
// https://dev.to/talhabalaj/setup-visual-studio-code-for-multi-file-c-projects-1jpi#setup2
// https://stackoverflow.com/questions/51720769/how-to-use-visual-studio-code-to-compile-multi-cpp-file

// Documenting functions (and tracepoints)
// https://marketplace.visualstudio.com/items?itemName=cschlosser.doxdocgen
// https://devblogs.microsoft.com/cppblog/visual-studio-code-c-extension-july-2020-update-doxygen-comments-and-logpoints/



#include <iostream>
#include "sumsub.h"

using namespace std;


int main(int argc, char *argv[])
{
    double num1, num2, sumOfNumbers, subOfNumbers;

    cout << "* Input First number : ";
	cin >> num1 ;

    cout << "* Input Second number : ";
	cin >> num2 ;

    sumAndSubPtr(num1,num2, &sumOfNumbers, &subOfNumbers);
    cout << endl << "Regarding " << num1 << " and " << num2 <<
            ", the sum is " << sumOfNumbers <<
            " and the difference is " << subOfNumbers << endl;

    sumAndSubRef(num1,num2, sumOfNumbers, subOfNumbers);
    cout << "Regarding " << num1 << " and " << num2 <<
            ", the sum is " << sumOfNumbers <<
            " and the difference is " << subOfNumbers << endl;

    // In the editor, place the mouse over the functions...

    return(0);
}




/*
#include <iostream>
#include <cstdlib>
using namespace std;

int main(int argc, char *argv[])
{
    double sum = 0;
    for(int counter = 0; counter < argc; counter ++)
    {
        sum += atof(argv[counter]); 
    }
    cout << "Sum = " << sum << endl;

    return(0);
}
*/


/*
void incrementByRef(int& n) { n += 1; }
void incrementByPtr(int* n) { *n += 1; }
int main() {
  int a = 0;
  incrementByRef( a); // In+Out parameter
  incrementByPtr(&a); // passes address
}
*/

