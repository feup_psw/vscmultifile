# Easy, naïve MultiFile project in VisualStudioCode (no makefile)

Written for the "Software Design" course of M.EEC, in FEUP, Portugal - Visual Studio Code Hello World in C and in C++ styles

Main Contributer: asousa@fe.up.pt

## License
Free software: Useful, transparent, no warranty

## What is it?

This initial project lets you have several *.cpp files Visual Studio Code under C++.

No object oriented classes, no overloading.

There is little Object Oriented in this project, only the insertion<< and extraction>> operators and **references**. References make code easier to read.

The application calculates and returns the sum and subtraction of two doubles in a single function.
There is a version of that function with pointers and another with references.


Features:
- [ main.cpp and sumsub.cpp ] are linked together into a forced executable name (library style / content separation); all *.cpp files are compiled and then linked; outputname is forced so that task can be run from any *.cpp file without changing target output filename
- input from the keyboard using extraction operator ````>>````
- subsum.h includes ````#ifndef SUBSUM_H````; also read [#pragma_once](https://en.wikipedia.org/wiki/Pragma_once)
- can be used as input redirection example via OS prompt ````<```` operator (see below notes on Power Shell)
- allows to easily compare pointers and references
- comments from the declarions are show on mouse hover above function name (doxigen style)


## Multifile in VSCode
Simply compiles and links together all *.cpp files in the folder, producing a fixed name executable. No makefile.

* To port this strategy to your own future project, in **tasks.json**, 
    * replace the entry   ````"${file}",````  with with the entry    ````"${fileDirname}\\*.cpp", ```` 
    * replace the entry ```` "${fileDirname}\\${fileBasenameNoExtension}.exe" ```` with the entry ````	"${fileDirname}\\ask_and_sum_and_subtract.exe" ````
    * In Linux and in both cases, forward // slashes 
    
* Note: it is still advocated to include 
    ````"-Wall",````
    in the compiler parameters


## Operating System input redirection

Redirection allows automatization.

In windows PowerShell, input redirection is a pain or not implemented.

In VSCode, under windows, compile, open terminal, write ````cmd```` to enter old command prompt and then run

        .\ask_and_sum_and_subtract.exe < .\inputfile 



## Support video(s)
(will appear here...)

## Tests
Tested in VSC 1.61, windows, g++ (Rev5, Built by MSYS2 project) 10.3.0


## Makefile
The makefile also works in windows powershell
Default option is build


README edited in 2021/10/13
