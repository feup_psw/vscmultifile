## Minimalistic makefile

default: build


INC=-I"include"



build:
	@g++ main.cpp sumsub.cpp -Wall $(INC) -lncurses -o ask_and_sum_and_subtract || (echo "\n==> $(tput setaf 1)Make failed.  Do you have all the src files?\n$(tput sgr0)"; exit 1)
	@echo "\n==> $(tput setaf 2)Make successful.  Start the game using $(tput sgr0)make run$(tput setaf 2) or $(tput sgr0)./pong$(tput sgr0)\n"

run:
	./pong || true

clean:
	rm ask_and_sum_and_subtract.exe
	rm ask_and_sum_and_subtract
